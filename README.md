# Radical Cobblemon Trainers

Over **700** unique and **challenging** trainers, from the Pokemon rom hack *Radical Red* (v3.02), that will spawn naturally in your world.

## Description

**Trainer spawning**: Encounter different trainers in different biomes.

![trainer_group](https://cdn.modrinth.com/data/lRwTUnD7/images/90fde6cb08864dd235c49405e74adfb313aa8bb8.png)

> Textures were procedurally generated in advance so trainer appearances do not resemble any specific figures.

---

**Story Mode**: Defeat leaders, rivals and many more to become the champion.

![trainer_battle_start](https://cdn.modrinth.com/data/lRwTUnD7/images/fc88a7cb14538738ca900eb965b74e40e7ecf42c.png)

---

![trainer_card_arrow](https://cdn.modrinth.com/data/lRwTUnD7/images/6fb1b46e6004992f928c19513e52e864bf945bd9.png)

---

**Player Progression**: Limits how far players can progress by preventing pokemon in their party, below a certain level (*level cap*), to gain any exp until they have defeated the next key trainer (e. g. leader).

![player_level_cap](https://cdn.modrinth.com/data/lRwTUnD7/images/0fbd04c6fbf1a3a7624015390f78447af5ae286d.png)

---

**Trainer Card**: The Trainer Card will help you to keep track of your progress, find information about trainers and to locate the next key trainer to increase your level cap, when they spawn nearby.

![trainer_card_recipe](https://cdn.modrinth.com/data/lRwTUnD7/images/29ec64c73cdea36e15885143c611d3f60d6e002a.png)

---

![trainer_card_list](https://cdn.modrinth.com/data/lRwTUnD7/images/7b8e14473b9b9258348299156bd5d3d8d421ad28.png)

---

**Endgame**: There are many challenging trainers for you to encounter, even after the champion. Seek out stronger trainers for better loot!

![trainer_silhouettes](https://cdn.modrinth.com/data/lRwTUnD7/images/87405f1ddcea4dd0db4c1a3a67603ec555cbff97.png)

---

**Native support for other mods**:

- [PKGBadges/CobbleBadges](https://modrinth.com/mod/pkgbadges): Leaders will drop badges from this mod
- [SimpleTMs](https://modrinth.com/mod/simpletms-tms-and-trs-for-cobblemon): Some trainers have a chance to drop a random TR (note: there is currently a *bug* where pokemon of any trainers also have a chance to drop TRs, you may disable the chance for this to happen in the SimpleTMs config)

## Configuration/Documentation

The mod contains many config options and has an extensive data pack support. Please refer to the [documentation](https://srcmc.gitlab.io/rct/docs) for more information, guides and the FAQ.

## Dependencies

- [Cobblemon](https://modrinth.com/mod/cobblemon)
- [Radical Cobblemon Trainers API](https://modrinth.com/mod/rctapi)
- [Forge Config API Port](https://modrinth.com/mod/forge-config-api-port) (**only on fabric**)

## Known Issues

- Trainers fail to spawn with 'Convenient Decors' installed
- I've had reports that some `Persistent` trainers randomly stopped accepting battles after a while (I could not reproduce this on my side yet)

## Planned features

- ~~More/Better/Fixed assets (Teams, Textures, Mobs, Dialogs, Loot tables, ...)~~
- Clear distinction between male and female trainers (textures)
- Clear distinction between adult and child trainers (models)
- ~~More config and datapack options to allow trainers beeing used in advancement like scenarios:~~
  - ~~disable despawning~~
  - ~~max trainer defeats/wins per player~~
  - ~~infinite trainer defeats/wins (e.g. by setting (global) max wins/defeats to `0`)~~
  - ~~loot condition for n'th (e.g. 1st) defeat (`defeat_count`)~~
- ~~Advancements~~
- ~~More user feedback in certain situations (e.g. when a player gains a badge)~~
- ~~Some config options~~
- ~~More commands~~
- ~~Fabric~~

## Discord

You can find a [Discord Channel](https://discord.com/channels/934267676354834442/1234121411275133031) for this mod in the [Cobblemon Discord Server](https://discord.gg/cobblemon).

## License

The **source code** of this project is licensed under [GNU-LGPL](LICENSE.md).
